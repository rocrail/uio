/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2025 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#include <Arduino.h>
#include "IO.h"
#include "Trace.h"

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

static Adafruit_NeoPixel* m_NP = NULL;
static PixelCtrl* m_Pixel = NULL;

extern char m_IO[MAX_BANKS][8];
extern const byte* m_Pin[MAX_BANKS];
extern byte m_RS485_Enable;


void IO::initIO( bool rs485, byte* state ) {
  m_NP = NULL;

  byte banks = MAX_BANKS;

  for( byte n = 0; n < banks; n++ ) {
    for( byte i = 0; i < 8; i++ ) {
      if( m_IO[n][i] == PORT_TYPE_INPUT || m_IO[n][i] == PORT_TYPE_BLOCK && m_Pin[n][i] != UNUSED )
        pinMode(m_Pin[n][i], INPUT_PULLUP);
      else if( m_Pin[n][i] != UNUSED ) {
        pinMode(m_Pin[n][i], OUTPUT);
        if( m_IO[n][i] == PORT_TYPE_OUTPUT || m_IO[n][i] == PORT_TYPE_PULSE || m_IO[n][i] == PORT_TYPE_iOUTPUT || m_IO[n][i] == PORT_TYPE_iPULSE ) {
          if( state != NULL ) {
            byte os = state[n*8+i];
            digitalWrite( m_Pin[n][i], os );
          }
        }
      }
    }
  }

  if( rs485 ) pinMode(m_RS485_Enable, OUTPUT);
}


void IO::modeRS485( bool rw ) {
  digitalWrite( m_RS485_Enable, rw ? HIGH:LOW );
}



void IO::initNP() {
  if( m_NP == NULL ) {
    m_Pixel = (PixelCtrl*)malloc( sizeof(struct _PixelCtrl) * NEOPIXEL_MAX );
    if( m_Pixel != NULL ) {
      trace("init NeoPixel");
      memset( m_Pixel, 0, sizeof(struct _PixelCtrl) * NEOPIXEL_MAX );
      m_NP = new Adafruit_NeoPixel(NEOPIXEL_MAX, NEOPIXEL_PIN, NEO_GRB + NEO_KHZ800);
      m_NP->begin();
      m_NP->clear();
    }
    else {
      trace("error init NeoPixel");
    }
  }
}


// NeoPixel Output
void IO::setPixel( byte pixel, byte bri, byte r, byte g, byte b, byte opt, byte nr ) {
  initNP();

  if( m_NP != NULL && pixel < NEOPIXEL_MAX ) {
    trace("NP:%d bri=%d (%d,%d,%d)", pixel, bri, r, g, b);
    if( nr == 0 ) nr = 1;
    for( byte i = pixel; i < NEOPIXEL_MAX && (i - pixel) < nr; i++ ) {
      m_Pixel[i].r      = r;
      m_Pixel[i].g      = g;
      m_Pixel[i].b      = b;
      m_Pixel[i].opt    = opt;
      //m_Pixel[i].bricur = 0;
      m_Pixel[i].brinew = bri;
      m_Pixel[i].bri    = bri;
      m_Pixel[i].flag   = FLAG_UPDATE;
      
      if( (opt & OPT_DELAY) == 0 ) {
        m_Pixel[i].bricur = bri;
      }
     }
  }
}


// NeoPixel Signal
void IO::doSignal(byte led, byte aspects, byte aspect, uint16_t aspectval, byte bri, byte dim) {
  initNP();

  if( m_NP != NULL && led < NEOPIXEL_MAX ) {
    byte r = 0;
    byte g = 0;
    byte b = 0;
    byte blink = 0;
    if( aspectval & 0x0001 ) {
      r = 0xFF;
      if( aspectval & 0x0100 )
        blink = OPT_BLINK;
    }
    if( aspectval & 0x0002 ) {
      g = 0xFF;
      if( aspectval & 0x0200 )
        blink = OPT_BLINK;
    }
    if( aspectval & 0x0004 ) {
      b = 0xFF;
      if( aspectval & 0x0400 )
        blink = OPT_BLINK;
    }
    
    m_Pixel[led].opt = (dim & 0x0F) + OPT_SIGNAL + blink;

    m_Pixel[led].r      = r;
    m_Pixel[led].g      = g;
    m_Pixel[led].b      = b;
    m_Pixel[led].bricur = 0;
    m_Pixel[led].brinew = bri;
    m_Pixel[led].bri    = bri;
    m_Pixel[led].flag   = FLAG_UPDATE;

    if( (m_Pixel[led].opt & OPT_DELAY) == 0 ) {
      m_Pixel[led].bricur = bri;
    }

    //m_NP->setPixelColor(led, m_NP->Color(round((r*bri)/255.0), round((g*bri)/255.0), round((b*bri)/255.0)) );
    //m_NP->show();


    if( aspects > 3 ) {
      IO::doSignal(led+1, 3, aspect, (aspectval>>3), bri, dim);
    }
  }

}

// 10ms
void IO::doPixels() {
  if( m_NP == NULL )
    return;

  bool update = false;
  
  for( byte i = 0; i < NEOPIXEL_MAX; i++ ) {
    if( (m_Pixel[i].bricur != m_Pixel[i].brinew) || m_Pixel[i].flag == FLAG_UPDATE || 
         (m_Pixel[i].opt & OPT_BLINK && m_Pixel[i].bri > 0)
         ) 
    {
      m_Pixel[i].flag = FLAG_NONE;

      byte delay = (m_Pixel[i].opt & OPT_DELAY);
      if( delay == 0 )
        delay = 1;

      int bricur = m_Pixel[i].bricur;
      int brinew = m_Pixel[i].brinew;
      // dim down
      if( bricur > brinew ) {
        bricur = bricur - delay;
        if( bricur < brinew )
          bricur = brinew;
      }

      // dim up
      else if( brinew > bricur ) {
        bricur = bricur + delay;
        if( bricur > brinew )
          bricur = brinew;
      }

      // check blink
      if( m_Pixel[i].opt & OPT_BLINK ) {
        if( bricur == brinew ) {
          if( brinew > 0 ) 
            m_Pixel[i].brinew = 0;
          else 
            m_Pixel[i].brinew = m_Pixel[i].bri;

        }
      }

      m_Pixel[i].bricur = (byte)bricur;
      float red   = m_Pixel[i].r;
      float green = m_Pixel[i].g;
      float blue  = m_Pixel[i].b;
      float bri   = m_Pixel[i].bricur;
      m_NP->setPixelColor(i, m_NP->Color(round((red*bri)/255.0), round((green*bri)/255.0), round((blue*bri)/255.0)) );

      update = true;
    }
  }
  
  if( update ) {
    m_NP->show();
  }
}