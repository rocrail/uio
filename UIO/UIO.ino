/* ================================================================================
 * Rocrail - Model Railroad Software
 * Copyright (c) 2002-2025 Robert Jan Versluis, Rocrail.net
 * All rights reserved.
 * ================================================================================
 */



#include <Arduino.h>
#include <Servo.h>
#include "RASCII_dfs.h"
#include "CV.h"
#include "IO.h"
#include "gen/revision.h"
#include "Trace.h"

#define REPORT_ON  1
#define REPORT_OFF 0

#define EOT "!\n\0"

#define SCANTIMER 50 // ms
#define BLOCK_TIMER (2000/SCANTIMER) // 40 * 50ms = 2000

#define BUFFERSIZE 16
static char packetBuffer[BUFFERSIZE][32];
char buffer[MAX_BUFFERSIZE];
static byte data[64];
static byte m_ID = 33;
static char m_NodeName[9]; // length 8 + terminating zero
static char cv[32];
static char val[32];
char m_IO[MAX_BANKS][8];

static byte m_State[MAX_IO];
static byte m_Timer[MAX_IO];

static byte m_Pulse = 5; // 5 * 100ms

const byte* m_Pin[MAX_BANKS];

byte m_RS485_Enable = 19;

static bool m_SOD = false;

static byte m_Model = 0;
static bool m_RS485 = false; // If set to false the event packets are stacked in the packetBuffer array.

static Servo* m_Servo[MAXSERVO];
static ServoCtrl m_ServoCtrl[MAXSERVO];

byte scanInput( byte pin, char type, byte idx) {
  byte state = SENS_OFF;
  
  if( pin == UNUSED ) {
    return state;
  }


  byte p = digitalRead(pin);
  if( p == LOW )
    state = SENS_ON;

  if( (m_SOD || m_State[idx] != state) && type == PORT_TYPE_INPUT ) {
    m_State[idx] = state;
    RASCII_inputEvent(idx, (state==SENS_ON) ? REPORT_ON:REPORT_OFF);
  }

  if( state == SENS_ON && m_State[idx] == SENS_OFF && type == PORT_TYPE_BLOCK ) {
    m_State[idx] = SENS_ON;
    m_Timer[idx] = BLOCK_TIMER;
    RASCII_inputEvent(idx, REPORT_ON);
  }
  else if( state == SENS_ON && m_State[idx] == SENS_ON && type == PORT_TYPE_BLOCK ) {
    m_Timer[idx] = BLOCK_TIMER; // reload timer
    if( m_SOD )
      RASCII_inputEvent(idx, (m_State[idx]==SENS_ON) ? REPORT_ON:REPORT_OFF);
  }
  else if( state == SENS_OFF && m_State[idx] == SENS_ON && type == PORT_TYPE_BLOCK ) {
    m_Timer[idx]--;
    if( m_Timer[idx] == 0 ) {
      m_State[idx] = SENS_OFF;
      RASCII_inputEvent(idx, REPORT_OFF);
    }
  }
  else if( m_SOD ) {
    RASCII_inputEvent(idx, (m_State[idx]==SENS_ON) ? REPORT_ON:REPORT_OFF );
  }

  return state;
}


void scan() {
  // Inputs
  byte banks = MAX_BANKS;

  for( byte n = 0; n < banks; n++ ) {
    for( byte i = 0; i < 8; i++ ) {
      byte index = i + n*8;
      if( m_IO[n][i] == PORT_TYPE_INPUT || m_IO[n][i] == PORT_TYPE_BLOCK ) {
        scanInput( m_Pin[n][i], m_IO[n][i], index);
      }

      // Outputs
      if( m_Pin[n][i] != UNUSED && (m_IO[n][i] == PORT_TYPE_PULSE || m_IO[n][i] == PORT_TYPE_iPULSE) && m_Timer[index] > 0 ) {
        m_Timer[index]--;
        if( m_Timer[index] == 0 ) {
          bool invert = (m_IO[n][i] == PORT_TYPE_iPULSE) ? true:false;
          digitalWrite( m_Pin[n][i], invert?HIGH:LOW );
          RASCII_outputEvent(index, LOW);
        }
      }
    }
  }

  for( byte i = 0; i < MAXSERVO; i++ ) {
    if( m_ServoCtrl[i].gotopos != m_ServoCtrl[i].curpos && m_ServoCtrl[i].steps == 0 ) {
      m_ServoCtrl[i].curpos = m_ServoCtrl[i].gotopos;
      m_Servo[i]->write( m_ServoCtrl[i].curpos ); // 0...180°
    } 
    else if( m_ServoCtrl[i].gotopos > m_ServoCtrl[i].curpos ) {
      m_ServoCtrl[i].curpos += m_ServoCtrl[i].steps;
      if( m_ServoCtrl[i].curpos > m_ServoCtrl[i].gotopos )
        m_ServoCtrl[i].curpos = m_ServoCtrl[i].gotopos;
      m_Servo[i]->write( m_ServoCtrl[i].curpos ); // 0...180°
    }
    else if( m_ServoCtrl[i].gotopos < m_ServoCtrl[i].curpos ) {
      m_ServoCtrl[i].curpos -= m_ServoCtrl[i].steps;
      if( m_ServoCtrl[i].curpos < m_ServoCtrl[i].gotopos )
        m_ServoCtrl[i].curpos = m_ServoCtrl[i].gotopos;
      m_Servo[i]->write( m_ServoCtrl[i].curpos ); // 0...180°
    }

    if( m_ServoCtrl[i].gotopos == m_ServoCtrl[i].curpos && m_ServoCtrl[i].report ) {
      m_ServoCtrl[i].report = false;
      RASCII_accessoryEvent( i, m_ServoCtrl[i].val, PORTTYPE_SERVO, m_ServoCtrl[i].curpos); 
    }

  }

  m_SOD = false;
}

void setOutput( byte port, byte value ) {
  if( port >= MAX_IO )
    return; // out of range

  byte bank  = port / 8;
  byte index = port - bank*8;
  if( m_Pin[bank][index] != UNUSED && 
      (m_IO[bank][index] == PORT_TYPE_OUTPUT || m_IO[bank][index] == PORT_TYPE_PULSE || m_IO[bank][index] == PORT_TYPE_iOUTPUT || m_IO[bank][index] == PORT_TYPE_iPULSE ) ) 
  {
    bool ival = value?true:false;
    if( m_IO[bank][index] == PORT_TYPE_iOUTPUT || m_IO[bank][index] == PORT_TYPE_iPULSE )
      ival = !ival;
    digitalWrite( m_Pin[bank][index], ival );
    m_State[port] = ival;
    if( value > 0 && (m_IO[bank][index] == PORT_TYPE_PULSE || m_IO[bank][index] == PORT_TYPE_iPULSE) ) {
      m_Timer[port] = (m_Pulse * 10) / SCANTIMER;
    }  
  }
}

void setSignal(byte port, byte aspects, byte aspect, int aspectval) {
  for( byte i = 0; i < aspects; i++ ) {
    int ledmask   = 0x0001 << i;

    byte val = (aspectval & ledmask  ) ? 1 : 0;

    trace("set signal output port %d to %d aspect=%d aspectval=%d", port+i, val, aspect, aspectval);
    setOutput(port+i, val);

  }

}


void RASCII_inputEvent(byte port, byte val) {
  data[0] = m_ID;
  data[1] = port;
  data[2] = val;
  RASCII_write(EVT_DIN, data, 3, EVT, false);
}


void RASCII_outputEvent(byte port, byte val) {
  data[0] = m_ID;
  data[1] = port;
  data[2] = val;
  RASCII_write(EVT_DOUT, data, 3, EVT, false);
}

//RASCII_accessoryEvent( i, m_ServoCtrl[i].val, PORTTYPE_SERVO, m_ServoCtrl[i].pos); 
void RASCII_accessoryEvent(byte port, byte val, byte ptype, uint32_t aspect) {
  data[0] = m_ID;
  data[1] = port;
  data[2] = val;
  data[3] = ptype;
  data[4] = (aspect >> 24) & 0xFF;
  data[5] = (aspect >> 16) & 0xFF;
  data[6] = (aspect >>  8) & 0xFF;
  data[7] = aspect & 0xFF;
  RASCII_write(EVT_ACCESSORY, data, 8, EVT, false);
}


void setup() {
  memset( m_Servo, 0, sizeof(m_Servo) );
  memset( m_ServoCtrl, 0, sizeof(m_ServoCtrl) );
  memset( m_State, 0, sizeof(m_State) );

  if( CV::get(OFFSET_EYE) != 'R' ) {
    CV::set( OFFSET_EYE, 'R' );
    CV::set( OFFSET_ID, m_ID );
    strcpy(m_NodeName, "myUIO");
    CV::setStr( OFFSET_NAME, m_NodeName, 8 );
    CV::set( OFFSET_PULSE, m_Pulse );
    CV::set( OFFSET_RS485, m_RS485 );
    for( byte i = 0; i < MAX_BANKS; i++ )
      CV::setStr( OFFSET_IO + i*8, "iiiiiiii", 8 );
    CV::setArray( OFFSET_IOSTATE, m_State, MAX_IO );
    CV::set( OFFSET_TAIL, 'R' );
  }
  else {
    m_ID = CV::get( OFFSET_ID );
    memset( m_NodeName, 0, sizeof(m_NodeName) );
    CV::getStr( OFFSET_NAME, m_NodeName );
    m_Pulse = CV::get( OFFSET_PULSE );
    if( m_Pulse == 0 ) m_Pulse = 5;
    m_RS485 = RS485_SUPPORT ? CV::get( OFFSET_RS485 ) : 0;

    memset( m_IO, 0, MAX_BANKS * 8 );
    for( byte i = 0; i < MAX_BANKS; i++ )
      CV::getStr( OFFSET_IO + i*8, m_IO[i] );

    if( CV::get(OFFSET_TAIL) != 'R' ) {
      CV::set( OFFSET_TAIL, 'R' );
      CV::setArray( OFFSET_IOSTATE, m_State, MAX_IO );
    }
    else {
      CV::getArray(OFFSET_IOSTATE, m_State, MAX_IO);
    }
  }

  if( MAX_BANKS > 0 ) m_Pin[0] = m_Pin1;
  if( MAX_BANKS > 1 ) m_Pin[1] = m_Pin2;
  if( MAX_BANKS > 2 ) m_Pin[2] = m_Pin3;
  if( MAX_BANKS > 3 ) m_Pin[3] = m_Pin4;
  if( MAX_BANKS > 4 ) m_Pin[4] = m_Pin5;
  if( MAX_BANKS > 5 ) m_Pin[5] = m_Pin6;
  if( MAX_BANKS > 6 ) m_Pin[6] = m_Pin7;
  if( MAX_BANKS > 7 ) m_Pin[7] = m_Pin8;
    

  Serial.begin(115200);
  delay(1000);

  #if defined(ARDUINO_AVR_UNO)
  const char* boardname = " UNO";
  m_Model = PRODUCTMODEL_UNO;
  #elif defined(ARDUINO_AVR_NANO)
  const char* boardname = " NANO";
  m_Model = PRODUCTMODEL_NANO;
  #elif defined(ARDUINO_AVR_MEGA2560)
  const char* boardname = " MEGA2560";
  m_Model = PRODUCTMODEL_MEGA;
  #elif defined(ARDUINO_AVR_MICRO)
  const char* boardname = " MICRO";
  m_Model = PRODUCTMODEL_MICRO;
  #else
  const char* boardname = " ?";
  m_Model = PRODUCTMODEL_UNKNOWN;
  #endif

  pinMode(LED_BUILTIN, OUTPUT);
  IO::initIO( m_RS485, m_State );
  if( m_RS485 ) IO::modeRS485(RS485_READ);

  trace("UIO starting up %d %s", m_ID, boardname);

}


static unsigned long m_10mS  = 0;
static unsigned long m_100mS = 0;
static unsigned long m_200mS = 0;

static unsigned long m_SCANmS = 0;

static bool m_LED = false;
static byte m_LEDtick = 0;

void loop() {
  unsigned long l_Millis = millis();

  if( l_Millis - m_10mS >= 10 ) {
    m_10mS = l_Millis;
    byte rc = RASCII_read();
    if( rc > 0 ) {
      evaluate(rc);
    }
    IO::doPixels();    
  }

  if( l_Millis - m_SCANmS >= SCANTIMER ) {
    m_SCANmS = l_Millis;
    scan();
  }

  if( l_Millis - m_100mS >= 100 ) {
    m_100mS = l_Millis;

    m_LEDtick++;
    if( m_LED && m_LEDtick >= 5 ) {
      m_LEDtick = 0;
      m_LED = false;
      digitalWrite(LED_BUILTIN, LOW);
    }
    if( !m_LED && m_LEDtick >= 2 ) {
      m_LEDtick = 0;
      m_LED = true;
      digitalWrite(LED_BUILTIN, HIGH);
    }
  }

  if( l_Millis - m_200mS >= 200 ) {
    m_200mS = l_Millis;
  }

  delay(1);
}


#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__

int freeMemory() {
  char top;
#ifdef __arm__
  return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
  return &top - __brkval;
#else  // __arm__
  return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__
}

static void showSettings() {
  //trace("ID=%d Name=%.8s IO1=%.8s IO2=%.8s Pulse=%d RS485=%d", m_ID, m_NodeName, m_IO1, m_IO2, m_Pulse, m_RS485);
  traceC("UIO %d", revisionnr);
  traceC("FreeHeap=%d", freeMemory() );
  trace("Configuration:");
  traceC("name=%s", m_NodeName );
  traceC("id=%d", m_ID);
  #if defined(ARDUINO_AVR_MEGA2560)
  traceC("i2c=%.8s%.8s %.8s%.8s %.8s%.8s %.8s%.8s", m_IO[0], m_IO[1], m_IO[2], m_IO[3], m_IO[4], m_IO[5], m_IO[6], m_IO[7] );
  #else
  traceC("i2c=%.8s%.8s", m_IO[0], m_IO[1]);
  traceC("pulse=%d", m_Pulse);
  #endif
}


static void setIO(byte bank) {
  if( bank < MAX_BANKS ) {
    memcpy( m_IO[bank], val, 8) ;
    CV::setStr( OFFSET_IO + bank * 8, m_IO[bank], 8 );
  }
}

static char iocnf[MAX_BANKS*16];
static void evaluate( byte rc ) {
  
  if( rc == RC_HLP ) {
    showSettings();
    return;
  }

  else if( rc == RC_CNF ) {
    if( strcmp(CV_ID, cv) == 0 ) {
      m_ID = atoi(val);
      CV::set( OFFSET_ID, m_ID );
    }
    else if( strcmp(CV_NAME, cv) == 0 ) {
      strcpy( m_NodeName, val) ;
      CV::setStr( OFFSET_NAME, m_NodeName, 8 );
    }
    else if( strcmp(CV_IO1, cv) == 0 ) {
      setIO(0);
    }
    else if( strcmp(CV_IO2, cv) == 0 ) {
      setIO(1);
    }
    else if( strcmp(CV_IO3, cv) == 0 ) {
      setIO(2);
    }
    else if( strcmp(CV_IO4, cv) == 0 ) {
      setIO(3);
    }
    else if( strcmp(CV_IO5, cv) == 0 ) {
      setIO(4);
    }
    else if( strcmp(CV_IO6, cv) == 0 ) {
      setIO(5);
    }
    else if( strcmp(CV_IO7, cv) == 0 ) {
      setIO(6);
    }
    else if( strcmp(CV_IO8, cv) == 0 ) {
      setIO(7);
    }
    else if( strcmp(CV_PULSE, cv) == 0 ) {
      m_Pulse = atoi(val);
      CV::set( OFFSET_PULSE, m_Pulse );
    }
    else if( strcmp(CV_RS485, cv) == 0 ) {
      m_RS485 = atoi(val);
      CV::set( OFFSET_RS485, m_RS485 );
      if( m_RS485 ) pinMode(m_RS485_Enable, OUTPUT);
    }
    showSettings();
  }

  else if( rc == RC_CMD ) {

    if( data[0] == CMD_QUERY ) {
      delay(m_ID * 4); // Broadcast: Delay response to prevent overlapping with other nodes.

      memset( data, 0, sizeof(data) );
      memset( iocnf, 0, sizeof(iocnf) );


      for( byte i = 0; i < MAX_BANKS; i++ ) {
        memcpy( iocnf+i*8, m_IO[i], 8);
      }


      data[ 0] = m_ID;
      data[ 1] = 'u';
      data[ 2] = MANUID_ROCRAIL;
      data[ 3] = PRODUCTID_ARDUINO | m_Model;
      data[ 4] = (revisionnr >> 8);
      data[ 5] = revisionnr & 0xFF;
      data[ 6] = 0;//options1;
      data[14] = 0;//options4;
      data[16] = 0;//options2;
      data[19] = m_Pulse;
      data[20] = 0;//options3;
      strcpy((char*)(data+21), iocnf);
      strcpy((char*)(data+21+1+strlen(iocnf)), m_NodeName);

      RASCII_write(CMD_QUERY, data, 21+strlen(iocnf)+1+strlen(m_NodeName)+1, RSP, true);
    }

    else if( data[0] == CMD_STOP ) {
      CV::setArray(OFFSET_IOSTATE, m_State, MAX_IO);
    }

    else if( data[0] == CMD_SLAVE_DATA ) {
      if( data[1] == m_ID ) {
        m_RS485 = true; // Stop sending directly.
        sendStack();
      }
    }

    else if( data[0] == CMD_SETWIO ) {
      if( data[1] == m_ID ) {
        byte index = data[2]; // iocnf index
        
        m_Pulse = data[3];
        CV::set( OFFSET_PULSE, m_Pulse );

        const char* conf = (const char*)(data+9); 
        int conflen = strlen(conf);
        if( index < MAX_BANKS ) {
          memcpy( m_IO[index], conf, 8 );
          CV::setStr( OFFSET_IO + index * 8, m_IO[index], 8 );
        }

        IO::initIO( m_RS485, NULL );
      }
    }

    else if( data[0] == CMD_SETIDTYPE ) {
      if( data[1] == m_ID ) {
        m_ID = data[2];
        CV::set( OFFSET_ID, m_ID );
        char* nn = (char*)(data+4);
        nn[8] = '\0'; // max. nodename length
        strcpy( m_NodeName, nn );
        CV::setStr( OFFSET_NAME, m_NodeName, 8 );
      }
    }
    
    else if( data[0] == CMD_SOD ) {
      trace("SoD %d", m_ID);
      m_SOD = true;
    }
    
    else if( data[0] == CMD_ACC_PAIR ) {
      if( data[1] == m_ID ) {
        byte addr  = data[1];
        byte port  = data[2];
        byte value = data[3];

        byte bank = port / 8;
        m_IO[bank][port-(bank*8)] = 'p'; 

        if( value == PORT_STATE_STRAIGHT ) {
          trace("accessory pair %d:%d+0 %d STRAIGHT", addr, port, value);
          setOutput(port, 1);
        }
        else {
          trace("accessory pair %d:%d+1 %d TURNOUT", addr, port, value);
          setOutput(port+1, 1);
        }



      }
    }

    else if( data[0] == CMD_SIGNAL ) {
      if( data[1] == m_ID ) {
        byte ledoffset = data[2];
        byte aspects   = data[3];
        byte aspect    = data[4];
        uint16_t aspectval = data[5] * 256 + data[6];
        byte bri       = data[7];
        byte dim       = data[8];
        byte ptype     = data[9];

        if( ptype == PORTTYPE_LED ) {
          trace("Pixel signal:%d %d:%d:0x%04X bri=%d", ledoffset, aspects, aspect, aspectval, bri);
          IO::doSignal(ledoffset, aspects, aspect, aspectval, bri, dim);
        }
        else {
          trace("Output signal:%d %d:%d:0x%04X", ledoffset, aspects, aspect, aspectval);
          setSignal(ledoffset, aspects, aspect, aspectval);
        }
      }
    }

    else if( data[0] == CMD_DOUT ) {
      if( data[1] == m_ID ) {
        byte addr  = data[1];
        byte port  = data[2];
        byte value = data[3];
        byte delay = data[4];
        byte blink = data[5];
        trace("output %d:%d+0 %d STRAIGHT", addr, port, value);
        setOutput(port, value);
      }
    }

    else if( data[0] == CMD_SERVO ) {
      if( data[1] == m_ID ) {
        byte addr  = data[1];
        byte servo = data[2];
        byte pos   = data[3];
        byte tick  = data[4];
        byte val   = data[5];
        if( servo < MAXSERVO ) {
          trace("servo %d pos=%d val=%d", servo, pos, val );
          if( m_Servo[servo] == NULL ) {
            m_IO[0][servo] = 'o';
            m_Servo[servo] = new Servo();
            m_Servo[servo]->attach( m_ServoPin[servo] );
          }
          //m_Servo[servo]->write(pos); // 0...180°
          m_ServoCtrl[servo].gotopos = pos;
          m_ServoCtrl[servo].steps   = tick;
          m_ServoCtrl[servo].val     = val;
          m_ServoCtrl[servo].report  = true;
        }
      }
    }

    else if( data[0] == CMD_AOUT ) {
      if( data[1] == m_ID ) {
        byte port = data[2];
        trace("Pixel %d:%d %d %d,%d,%d", m_ID, port, data[3], data[4], data[5], data[6]);
                        // bri      red      green    blue     opt      nr
        IO::setPixel(port, data[3], data[4], data[5], data[6], data[7], data[8]);
      }
    }
  


  }
}


static byte RASCII_read() {
  if( m_RS485 ) digitalWrite( m_RS485_Enable, LOW );
  while( Serial.available() ) {
    buffer[0] = 0;
    Serial.readBytes(buffer, 1);
    char c = buffer[0];
    //Serial.print("read " + c);

    if( c == QID ) {
      delay(m_ID * 4); // Broadcast: Delay response to prevent overlapping with other nodes.
      data[0] = m_ID;
      strcpy((char*)(data+1), m_NodeName);
      RASCII_write(EVT_IDENT, data, 1+strlen(m_NodeName)+1, EVT, true);
      m_SOD = true;
      return RC_NONE;
    }

    if( c == CMD || c == RSP || c == EVT ) {
      char hexa[2];
      if( Serial.readBytes(hexa, 2) == 2 ) {
        byte len = HexA2Byte(hexa);
        if( Serial.readBytes(buffer, len) == len ) {
          buffer[len] = '\0';
          for( byte i = 0; i < len; i++ ) {
            data[i] = HexA2Byte(buffer+i*2);
          }
          if( c == EVT )
            return RC_EVT;
          return (c == CMD ? RC_CMD:RC_RSP);
        }
      }
      return RC_NONE;
    }

    if( c == HLP ) {
      return RC_HLP;
    }
    
    if( c == DMP ) {
      return RC_DMP;
    }

    else if( c == CNF ) {
      char* p = cv;
      byte idx = 0;
      while( idx < 64 ) {
        Serial.readBytes(&c, 1);
        if( c == '=' ) {
          idx = 0;
          p = val;
          p[idx] = '\0';
          continue;
        }
        if( c == '\n' || c == '\r' || c == '\0' ) {
          break;
        }
        p[idx] = c;
        idx++;
        p[idx] = '\0';
      }
      return RC_CNF;
    }

  }

  return RC_NONE;
}


byte HexA2Byte( const char* s ) {
  byte h, l, val;
  if( s[0] >= 'a' ) h = s[0] - 87;
  else if( s[0] >= 'A' ) h = s[0] - 55;
  else h = s[0] -48;
  if( s[1] >= 'a' ) l = s[1] - 87;
  else if( s[1] >= 'A' ) l = s[1] - 55;
  else l = s[1] -48;
  val = l + (h << 4);
  return val;
}

char* Byte2HexA(byte b, char* c) {
  static char cHex[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
  c[0] = cHex[(b&0xF0)>>4];
  c[1] = cHex[ b&0x0F    ];
  c[2] = '\0';
  return c;
}

void RASCII_write(byte evt, byte* data, byte len, byte type, bool force) {
  byte idx = 0;
  // Type @,#,$,&
  buffer[0] = type;
  idx++;

  // Length
  Byte2HexA((len+1)*2, buffer+idx);
  idx+=2;

  // Command, Event
  Byte2HexA(evt, buffer+idx);
  idx+=2;

  // Data
  for( byte i = 0; i < len; i++ ) {
    Byte2HexA(data[i], buffer+idx);
    idx+=2;
  }
  buffer[idx+0] = '\n';
  buffer[idx+1] = '\0';
  idx++;
  if( !m_RS485 || force ) {
    if( m_RS485 ) digitalWrite( m_RS485_Enable, HIGH );
    Serial.write(buffer, idx);
    delay(1);
  }
  else {
    for( byte i = 0; i < BUFFERSIZE; i++ ) {
      if( packetBuffer[i][0] == 0 ) {
        strcpy( packetBuffer[i], buffer );
        break;
      }
    }
  }
}

// Send just one.
void sendStack() {
  if( m_RS485 ) digitalWrite( m_RS485_Enable, HIGH );
  for( byte i = 0; i < BUFFERSIZE; i++ ) {
    if( packetBuffer[i][0] != 0 ) {
      Serial.write(packetBuffer[i], strlen(packetBuffer[i]));
      packetBuffer[i][0] = 0;
      return;
    }
  }
  Serial.print("#nop\n");
}
