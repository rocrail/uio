/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2025 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#ifndef _CV_H_
#define _CV_H_

#define OFFSET_EYE     0
#define OFFSET_ID      1
#define OFFSET_NAME    2 // max. length 8 chars + 1 zero
#define OFFSET_PULSE   10
#define OFFSET_RS485   11
#define OFFSET_IO      12 // 8 chars per bank, max. 64 bytes
#define OFFSET_IOSTATE 12 + 64 // 8 bits per bank, max. 8 bytes
#define OFFSET_TAIL    OFFSET_IOSTATE + 64

#define MANUID_ROCRAIL       70
#define PRODUCTID_ARDUINO    5
#define PRODUCTMODEL_NANO    0x00
#define PRODUCTMODEL_UNO     0x10
#define PRODUCTMODEL_MEGA    0x20
#define PRODUCTMODEL_MICRO   0x30
#define PRODUCTMODEL_UNKNOWN 0xF0

#define CV_ID       "id"
#define CV_NAME     "name"
#define CV_IO1      "io1"
#define CV_IO2      "io2"
#define CV_IO3      "io3"
#define CV_IO4      "io4"
#define CV_IO5      "io5"
#define CV_IO6      "io6"
#define CV_IO7      "io7"
#define CV_IO8      "io8"
#define CV_PULSE    "pulse"
#define CV_RS485    "rs485"

class CV
{
private:

public:
  CV();
  static void resetAll();
  static unsigned char get(unsigned int  cv);
  static unsigned int  getReg(unsigned int  cv);
  static void getStr(unsigned int offset, char* value);
  static void set(unsigned int cv, unsigned char value);
  static void setReg(unsigned int cv, unsigned int value);
  static void setStr(unsigned int offset, const char* value, byte len);
  static void setArray(unsigned int offset, const byte* value, byte len);
  static void getArray(unsigned int offset, byte* value, byte len);
};

#endif
