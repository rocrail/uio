/* ================================================================================
 * Rocrail - Model Railroad Software
 * Copyright (c) 2002-2025 Robert Jan Versluis, Rocrail.net
 * All rights reserved.
 * ================================================================================
 */

#ifndef RASCII_dfs_h
#define RASCII_dfs_h

#define CMD '@'
#define EVT '$'
#define INF '#'
#define RSP '&'
#define CNF '*'
#define HLP '?'
#define DMP '!'
#define BUS ':'
#define QID '%'

#define RC_NONE 0
#define RC_CMD  1
#define RC_CNF  2
#define RC_HLP  3
#define RC_RSP  4
#define RC_DMP  5
#define RC_EVT  6


#define CMD_INFO        0
#define CMD_START       1
#define CMD_STOP        2
#define CMD_GETCONF     3
#define CMD_SETCONF     4
#define CMD_RESETCONF   5
#define CMD_GETCV       6
#define CMD_SETCV       7
#define CMD_DOUT        10
#define CMD_AOUT        11
#define CMD_ACCESSORY   12
#define CMD_MOBILE_DIRV 13
#define CMD_MOBILE_FUN  14
#define CMD_MOBILE_DISPATCH 15
#define CMD_MOBILE_RELEASE  16
#define CMD_EBREAK          17
#define CMD_SOD             18
#define CMD_QUERY           19
#define CMD_SETIDTYPE       20
#define CMD_SETWIO          21
#define CMD_MOBILE_BIND     22

#define CMD_SERVO           23
#define CMD_TEXT            24
#define CMD_CLOCK           25
#define CMD_SIGNAL          26
#define CMD_SWITCH          27
#define CMD_BINSTATE        28
#define CMD_MOBILE_SHORTID  29
#define CMD_CLEAR_SHORTIDS  30
#define CMD_END_SHORTIDS    31
#define CMD_SHUTDOWN        32
#define CMD_MOBILE_STATE    33
#define CMD_SOUND_PLAY      34
#define CMD_OTA             35
#define CMD_REBOOT          36
#define CMD_PT_ON           37
#define CMD_PT_OFF          38
#define CMD_ACC_GETCV       39
#define CMD_ACC_SETCV       40
#define CMD_ACC_PAIR        41
#define CMD_MOTOR           42
#define CMD_SLEEP           43
#define CMD_SHOW            44 // Identify by flashing LED(s)
#define CMD_STARTCAM        45
#define CMD_STOPCAM         46
#define CMD_LOCATION        47
#define CMD_GET_IOCNF       48 // First data is the index: 0=boardIO 1=0...15, 2=16...31, ...
#define CMD_SET_IOCNF       49
#define CMD_SYSUPDATE       50
#define CMD_GETOPTION       51
#define CMD_SETOPTION       52
#define CMD_MACRO           53
#define CMD_MACRO_COLOR     54
#define CMD_HALL_BEGIN      55
#define CMD_HALL_END        56
#define CMD_HALL_REPORT     57
#define CMD_SERVO_PWM       58

#define CMD_SLAVE_DATA     100 // RS485 poll command; WIO with NodeID x may send events.
#define CMD_SLAVE_EOD      101 // RS485 'End Of Data' response. Send as last package by the selected slave.

#define EVT_ALIVE 0
#define EVT_OCC   1
#define EVT_RFID  2
#define EVT_DIN   3
#define EVT_AIN   4
#define EVT_DSEN  5
#define EVT_ASEN  6
#define EVT_DOUT  7
#define EVT_ACCESSORY    8
#define EVT_SHORTCIRCUIT 9
#define EVT_BIDI         10
#define EVT_SENSORID     11
#define EVT_STATE        12
#define EVT_MOBILE_STATE 13
#define EVT_PT           14
#define EVT_IO           15
#define EVT_KMH          16
#define EVT_DINS         17
#define EVT_IDENT        253
#define EVT_GOODBYE      255


#define PORTTYPE_DOUT  0
#define PORTTYPE_AOUT  1
#define PORTTYPE_SERVO 2
#define PORTTYPE_PAIR  3
#define PORTTYPE_MOTOR 4
#define PORTTYPE_DIN   5
#define PORTTYPE_ACC   6
#define PORTTYPE_LED   7
#define PORTTYPE_MULTIPLEX 10

#define PORT_STATE_STRAIGHT 0
#define PORT_STATE_TURNOUT  1

#define RFID_LENGTH_MASK  0x0F
#define RFID_STATE_OFF    0x80
#define RFID_STATE_ERROR  0x40

#define PRODUCTID_MASK      0x0F
#define PRODUCTMODEL_MASK   0xF0
#define PRODUCTMODEL_RPI1   0x00
#define PRODUCTMODEL_ZERO1  0x10
#define PRODUCTMODEL_ZERO2  0x20
#define PRODUCTMODEL_RPI2   0x30
#define PRODUCTMODEL_RPI3   0x40
#define PRODUCTMODEL_RPI4   0x50
#define PRODUCTMODEL_PICO   0x60

#endif
