/* ================================================================================
 * Rocrail - Model Railroad Software
 * Copyright (c) 2002-2025 Robert Jan Versluis, Rocrail.net
 * All rights reserved.
 * ================================================================================
 */

#ifndef Trace_h
#define Trace_h


void trace(const char* fmt, ...);
void traceC(const char* fmt, ...);


#endif

