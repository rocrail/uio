/* ================================================================================
 * Rocrail - Model Railroad Software
 * Copyright (c) 2002-2025 Robert Jan Versluis, Rocrail.net
 * All rights reserved.
 * ================================================================================
 */



#include <Arduino.h>

extern char buffer[128];

void trace(const char* fmt, ...) {
  memset( buffer, 128, 0 );
  buffer[0] = '#';
  va_list args;
  va_start(args, fmt);
  vsnprintf(buffer+1, 128, fmt, args);
  va_end(args);
  Serial.println(buffer);
}


void traceC(const char* fmt, ...) {
  memset( buffer, 128, 0 );
  va_list args;
  va_start(args, fmt);
  vsnprintf(buffer, 128, fmt, args);
  va_end(args);
  Serial.println(buffer);
}


