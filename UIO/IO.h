/*
 Rocrail - Model Railroad Software
 Copyright (c) 2002-2025 Robert Jan Versluis, Rocrail.net
 All rights reserved.
*/
#ifndef _IO_H_
#define _IO_H_

#define RS485_SUPPORT false
#define RS485_READ    false
#define RS485_WRITE   true

#define SENS_OFF HIGH
#define SENS_ON  LOW

#define PORT_TYPE_INPUT   'i'
#define PORT_TYPE_BLOCK   'b'
#define PORT_TYPE_OUTPUT  'o'
#define PORT_TYPE_PULSE   'p'
#define PORT_TYPE_iOUTPUT 'O'
#define PORT_TYPE_iPULSE  'P'


#if defined(ARDUINO_AVR_MEGA2560)
  #define MAX_IO    64
  #define MAX_BANKS 8
  #define MAX_BUFFERSIZE 256
#else
  #define MAX_IO    16
  #define MAX_BANKS 2
  #define MAX_BUFFERSIZE 128
#endif

#define UNUSED 255

// Nano/Uno pins: (BuildIn LED is 13)
const byte m_Pin1[8] = { 2, 3, 4, 5, 6, 7, 8, 9 };
const byte m_Pin2[8] = { 10,11,12,14,15,16,17,18};
// Mega extra pins:
const byte m_Pin3[8] = { 20,21,22,23,24,25,26,27};
const byte m_Pin4[8] = { 28,29,30,31,32,33,34,35};
const byte m_Pin5[8] = { 36,37,38,39,40,41,42,43};
const byte m_Pin6[8] = { 44,45,46,47,48,49,50,51};
const byte m_Pin7[8] = { 52,53,54,55,56,57,58,59};
const byte m_Pin8[8] = { 60,61,62,63,64,65,66,67};

#define MAXSERVO 8
const byte m_ServoPin[MAXSERVO] = { 2, 3, 4, 5, 6, 7, 8, 9 };

#if defined(ARDUINO_AVR_MEGA2560)
  #define NEOPIXEL_PIN 19
  #define NEOPIXEL_MAX 64
#else
  #define NEOPIXEL_PIN 19
  #define NEOPIXEL_MAX 8
#endif

typedef struct __ServoCtrl {
  byte gotopos;
  byte curpos;
  byte steps;
  byte val;
  bool report;
} ServoCtrl;


typedef struct _PixelCtrl {
  byte r;
  byte g;
  byte b;
  byte opt;
  byte bri;
  byte bricur;
  byte brinew;
  byte flag;
  uint16_t val; // aspect value
} PixelCtrl;

#define OPT_SIGNAL  0x20
#define OPT_BLINK   0x10
#define OPT_DELAY   0x0F

#define FLAG_NONE         0
#define FLAG_UPDATE       1


class IO
{
static void initNP();

public:
  static void initIO( bool rs485, byte* state );
  static void modeRS485( bool rw );
  static void setPixel( byte pixel, byte bri, byte r, byte g, byte b, byte opt, byte nr );
  static void doSignal(byte led, byte aspects, byte aspect, uint16_t aspectval, byte bri, byte dim);
  static void doPixels();
};

#endif
