#!/bin/sh
echo "const int revisionnr = " > UIO/gen/revision.h
git rev-list --count HEAD >> UIO/gen/revision.h
echo ";" >> UIO/gen/revision.h

git log -n 500 --abbrev-commit --oneline --format="%cs %s" | awk -v REV=`git rev-list --count HEAD` '{printf"\"%d %s\",\n",REV,$0;REV=REV-1;}' > UIO/gen/log.txt